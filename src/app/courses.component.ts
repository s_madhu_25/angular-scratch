import {Component} from '@angular/core';
import { CoursesService } from './courses.service';

@Component({
    //<courses> "courses"
    //<div class="courses"> ".courses"
    //<div id="courses" > "#courses"

    //directives - used to manipulate the dom, add dom element, remove, change styling
                    // directives should be prefixed by an *
    selector : 'courses',
    template : `<h2>{{getMsg()+ " Topic : "+title }}</h2>
                <ul>
                <li *ngFor="let course of courses">{{course}}</li>
                </ul>
                `
})
export class coursesComponent{

    title = "list of courses";
    courses;

    constructor(service : CoursesService)  //dependency injection 
    {
        
        this.courses = service.getCourses();
    }

    getMsg()
    {
        return "Hello";
    }



}