import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmpInfoComponent } from './emp-info/emp-info.component';
import { FormcomponentComponent } from './formcomponent/formcomponent.component';
import { NavbarComponent } from './navbar/navbar.component';

const routes: Routes = [{path : 'register' , component: FormcomponentComponent}, 
{path: 'details' , component : EmpInfoComponent},
{path:'' , component: NavbarComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
